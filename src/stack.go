package main

type queueStack struct {
	QS []string
}

func (qs *queueStack) Pop() string {
	var (
		 cnt  int
		result string
	)
	cnt = len(qs.QS)
	result = qs.QS[0];
	if (cnt == 1) {
		qs.QS = make([]string,0)
	}else {
		qs.QS = qs.QS[1:]
	}
	return result
}
//функции для работы как со стеком
func (qs *queueStack) Spush(el string) {
	tempSlice := make([]string, len(qs.QS)+1)
	tempSlice[0] = el
	for i := range qs.QS {
		tempSlice[i+1] = qs.QS[i]
	}
	qs.QS = tempSlice
}

//функции для работы с очередью
func (qs *queueStack) QPush(el string) {
	tempSlice := make([]string, len(qs.QS)+1)
	for i := range qs.QS {
		tempSlice[i] = qs.QS[i]
	}
	tempSlice[len(qs.QS)] = el
	qs.QS = tempSlice
}
