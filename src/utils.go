package main

import (
	goCharset "code.google.com/p/go-charset/charset"
	"strings"
	"io/ioutil"
	"fmt"
	"regexp"
	"strconv"
	"github.com/mostafah/mandrill"
	"time"
)

func error_log(err error) {
	if err != nil {
		mandrill.Key = apiKeyForMandrill

		msg := mandrill.NewMessageTo(tomail, "Parser Admin")

		msg.FromEmail = "parserToys@forOurtoys.org"
		msg.FromName = "Parse"

		msg.Text = err.Error() + " time: "+ time.Now().Local().String()// optional
		msg.Subject = "Parser get error for target: " + target +" time: "+ time.Now().Local().String()
		msg.Send(true);
	}
}

func encode_string(str string, charset string) string {
	r, err := goCharset.NewReader(charset, strings.NewReader(str))
	error_log(err)
	result, err := ioutil.ReadAll(r)
	error_log(err)
	fmt.Printf("%s\n", result)
	return string(result)
}

func createFloatPrice (str string) float64 {
	var price float64
	replaceChars, err := regexp.Compile("[^\\d\\,\\.]+")
	error_log(err)
	replaceComma, err := regexp.Compile(",")
	error_log(err)
	
	str = replaceChars.ReplaceAllString(str,"")
	str = replaceComma.ReplaceAllString(str,".")
	price,  _ = strconv.ParseFloat(str, 64)
	return price
}

func removeTextWithRegExp(str string, regxp string) string{
	reg, err := regexp.Compile(regxp)
	error_log(err)
	result := reg.ReplaceAllString(str,"")
	return result
}
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
