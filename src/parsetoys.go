package main

import (
	"fmt"
	gq "github.com/PuerkitoBio/goquery"
	"io/ioutil"
	"flag"
	"encoding/json"
	"sync"
	"time"
	"labix.org/v2/mgo"
	"github.com/mostafah/mandrill"

)

type GlobalSet struct {
	Donors []Donor `json:"Donors"`
}

type Donor struct {
	Code                  string `json:"Code"`
	MainUrl               string `json:"MainUrl"`
	CurrentLink           string `json:"CurrentLink"`
	ProductName           []string `json:"ProductName"`
	NewProduct            []string  `json:"NewProduct"`
	HitProduct            []string  `json:"HitProduct"`
	Price                 []string  `json:"Price"`
	Articul               []string  `json:"Articul"`
	Sex                   []string  `json:"Sex"`
	Age                   []string  `json:"Age"`
	Description           []string  `json:"Description"`
	Pictures              []string `json:"Pictures"`
}

type Tovar struct {
	Site         string
	Link         string
	NameProduct  string
	Action_acia  int
	Action_new   int
	Price        float64
	Art          string
	Sex          string
	Age          string
	Descrip      string
	Pic          []string
}

var (
	settingsFile string
	mongoURL string
	wg sync.WaitGroup
	apiKeyForMandrill string
	tomail string
	target string
	mongoSession *mgo.Session
	stack queueStack
)

func crawler(donor Donor) {
	wg.Add(1)
	stack.Spush(donor.CurrentLink)
	for{
		donor.CurrentLink = stack.Pop();
		if len(donor.CurrentLink) > 0 {
			if donor.CurrentLink[0] == '/' {
				if (!inBaseDonor(donor.CurrentLink)) {
					doc, err := gq.NewDocument(donor.MainUrl + donor.CurrentLink)
					if (err != nil) {
						error_log(err)
					}else {
						doc.Find("a").Each(func(i int, s *gq.Selection) {
							link, _ := s.Attr("href")
							if !stringInSlice(link, stack.QS) && !inBaseDonor(link){
								stack.Spush(link)
								fmt.Printf("Link: %v\n", link)
							}
						});
						addVisitedLinks(donor)
						getAndSaveTovarIfExist(doc, donor);
					}
				}
			}
		}
		if len(stack.QS)==0{
			break
		}
	}
	wg.Done()
}


func getDataFromDOM(s *gq.Selection, arr []string, code string) string {
	var dt string
	if (arr[0] == "text") {
		dt = s.Text()
	}else {
		dt, _ = s.Attr(arr[0])
	}
	return encode_string(dt, code)
}

func getAndSaveTovarIfExist(doc *gq.Document, donor Donor) {
	var curTovar Tovar
	doc.Find(donor.ProductName[1]).Each(func(i int, s *gq.Selection) {
		curTovar.NameProduct = getDataFromDOM(s, donor.ProductName, donor.Code)
	})
	doc.Find(donor.NewProduct[1]).Each(func(i int, s *gq.Selection) {
		if (getDataFromDOM(s, donor.NewProduct, donor.Code) == donor.NewProduct[2]) {
			curTovar.Action_new = 1
		}else {
			curTovar.Action_new = 0
		}
	})
	doc.Find(donor.HitProduct[1]).Each(func(i int, s *gq.Selection) {
		if (getDataFromDOM(s, donor.HitProduct, donor.Code) == donor.HitProduct[2]) {
			curTovar.Action_acia = 1
		}else {
			curTovar.Action_acia = 0
		}
	})
	doc.Find(donor.Price[1]).Each(func(i int, s *gq.Selection) {
		curTovar.Price = createFloatPrice(getDataFromDOM(s, donor.Price, donor.Code))
	})
	doc.Find(donor.Articul[1]).Each(func(i int, s *gq.Selection) {
		curTovar.Art = removeTextWithRegExp(getDataFromDOM(s, donor.Articul, donor.Code), donor.Articul[2])
	})
	doc.Find(donor.Sex[1]).Each(func(i int, s *gq.Selection) {
		curTovar.Sex = getDataFromDOM(s, donor.Sex, donor.Code)
	})
	doc.Find(donor.Age[1]).Each(func(i int, s *gq.Selection) {
		curTovar.Age = getDataFromDOM(s, donor.Age, donor.Code)
	})
	doc.Find(donor.Description[1]).Each(func(i int, s *gq.Selection) {
		curTovar.Descrip = getDataFromDOM(s, donor.Description, donor.Code)
	})
	doc.Find(donor.Pictures[1]).Each(func(i int, s *gq.Selection) {
		curTovar.Pic = append(curTovar.Pic, donor.MainUrl+getDataFromDOM(s, donor.Pictures, donor.Code))
	})
	if (curTovar.Art != "") {
		curTovar.Site = donor.MainUrl
		curTovar.Link = donor.CurrentLink
		fmt.Printf("Название: %+v\n", curTovar.NameProduct)
		fmt.Printf("Новый: %+v\n", curTovar.Action_acia)
		fmt.Printf("Хит: %+v\n", curTovar.Action_new)
		fmt.Printf("Цена: %+v\n", curTovar.Price)
		fmt.Printf("Артикул: %+v\n", curTovar.Art)
		fmt.Printf("Пол: %+v\n", curTovar.Sex)
		fmt.Printf("Возраст: %+v\n", curTovar.Age)
		fmt.Printf("Описание: %+v\n", curTovar.Descrip)
		fmt.Printf("Картинки: %+v\n", curTovar.Pic)
		wg.Add(1)
		go saveTovar(curTovar)
	}
}

func init() {
	flag.StringVar(&settingsFile, "setfile", "/data/donors.json", "File settings")
	flag.StringVar(&mongoURL, "mdb", "127.0.0.1:27017", "Connect MongoDB")
	flag.StringVar(&apiKeyForMandrill, "api", "", "Api Key for mandril service")
	flag.StringVar(&tomail, "tomail", "", "mail to send info ")
	flag.StringVar(&target, "target", "", "targer for parse")
}

func main() {
	var gs GlobalSet
	flag.Parse()

	mandrill.Key = apiKeyForMandrill

	msg := mandrill.NewMessageTo(tomail, "Parser Admin")
	msg.FromEmail = "parserToys@forOurtoys.org"
	msg.FromName = "Parse"

	msg.Text = "start parse for: " + target +" time: "+ time.Now().Local().String()
	msg.Subject = "Start parse for target "+ target +" time: "+ time.Now().Local().String()
	msg.Send(false);

	mongoSession = createMongoSession()
	defer mongoSession.Close()

	dropOldLinks()
	text, err := ioutil.ReadFile(settingsFile)
	err = json.Unmarshal(text, &gs)
	error_log(err)
	for _, v := range gs.Donors {
		go crawler(v)
	}
	time.Sleep(10 * time.Second)
	wg.Wait()

	msg.Text = "End parse for target: "+target+". No errors." + " time: "+ time.Now().Local().String()
	msg.Subject = "End parse for target "+target+" time: "+ time.Now().Local().String()
	msg.Send(false);


}
