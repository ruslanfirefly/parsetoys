package main

import (
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)
const (
	DBMNG string = "parsetoys"
	COLLINK string = "visitedlinks"
	COLTOYS string = "toysinfo"
)

func createMongoSession() *mgo.Session{
	session, err := mgo.Dial(mongoURL)
	error_log(err)
	return session
}

func dropOldLinks(){
	listCol, err := mongoSession.DB(DBMNG).C(COLLINK).Count()
	error_log(err)
	if(listCol != 0){
		err = mongoSession.DB(DBMNG).C(COLLINK).DropCollection()
		error_log(err)
	}

}

func inBaseDonor(link string) bool{
	listRes, err := mongoSession.DB(DBMNG).C(COLLINK).Find(bson.M{"currentlink":link}).Count()
	error_log(err)
	if (listRes == 0){
		return false
	}else{
		return true
	}
}
func inBaseTovar(tovar Tovar) bool{
	listRes, err := mongoSession.DB(DBMNG).C(COLTOYS).Find(bson.M{"site":tovar.Site,"art":tovar.Art}).Count()
	error_log(err)
	if (listRes == 0){
		return false
	}else{
		return true
	}
}
func addVisitedLinks(donor Donor){
	err := mongoSession.DB(DBMNG).C(COLLINK).Insert(donor)
	error_log(err)
}

func saveTovar(tovar Tovar){
	var err error
	if(inBaseTovar(tovar)){
		err = mongoSession.DB(DBMNG).C(COLTOYS).Update(bson.M{"site":tovar.Site,"art":tovar.Art},tovar)
	}else{
		err = mongoSession.DB(DBMNG).C(COLTOYS).Insert(tovar)
	}
	error_log(err)
	wg.Done()
}
